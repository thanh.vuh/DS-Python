
# coding: utf-8

# ### Objective
# ##### 1. Explore bus stop data of San Francisco
# ##### 2. Vizualize the bustop data and employee address on a map
# ##### 3. Provide insight from visualization
# 
# 

# ### Data Sources
# “Potential_Bust_Stops”: the list of potential bus stops. It is a list of intersections in the city. Columns:
# 
# •	“Street_One”: one of two streets intersecting
# 
# •	“Street_Two”: the other street intersecting
# 
# “Employee_Addresses”: the list of home address of each employee that in interested in taking buses. Columns:
# 
# •	“address”: employee address
# 
# •	“employee_id”: employee id, unique by employee

# ### Import library, data and data exploration

# In[1]:


#Import library Folium, Pandas
import folium
import pandas as pd

#Import data from csv file
busstop = pd.read_csv('Potentail_Bust_Stops.csv')
employee = pd.read_csv('Employee_Addresses.csv')


# In[2]:


#Check 5 top and bottom rows
print (busstop.head())
print(busstop.tail())
#Check data infomation
busstop.info()

#check number of Null value
busstop.isnull().sum()


# In[3]:


#Check 5 top and bottom rows
print(employee.head())
print(employee.tail())

#Check data infomation
employee.info()

#check number of Null value
employee.isnull().sum()


# In[4]:


#Replace value "ST" to "street" for more accurate in finding latitude and longtitude
busstop["Street_Two"]=busstop["Street_Two"].str.replace(" ST"," Street")


# ### Draw map of San Francisco using Folium

# In[5]:


#Import Geopy library using under Google API
from geopy.geocoders import GoogleV3
city="San Francisco, CA, USA"
geolocator = GoogleV3(api_key="AIzaSyD_sVpnDsJ8thEGRjhHrow2EyNswtRBmiw")
location = geolocator.geocode(query=city)
SFC_map = folium.Map((location.latitude, location.longitude), zoom_start=13)

SFC_map


# In[6]:


# Scraping latitude and longitude of each intersection from Google Geocoding API
latitudeBus=[]
longitudeBus=[]
geolocator = GoogleV3(api_key="AIzaSyD_sVpnDsJ8thEGRjhHrow2EyNswtRBmiw")
for i in range(0, busstop.Street_Two.count()):
    location = geolocator.geocode(query=busstop.Street_One[i]+" & "+busstop.Street_Two[i]+" "+city, timeout=2)
    latitudeBus.append(location.latitude)
    longitudeBus.append(location.longitude)
# Insert latitude and longtitude back to busstop data to vizualize on the map
latitudeBus=pd.Series(latitudeBus)
busstop = pd.concat((busstop, latitudeBus.rename('Latitude')), axis=1)
longitudeBus=pd.Series(longitudeBus)
busstop = pd.concat((busstop, longitudeBus.rename('Longitude')), axis=1)


# In[7]:


#Draw a map with each intersection is marked by a blue icon
for i in range(0, busstop.Latitude.count()):
    folium.Marker([busstop.Latitude[i],busstop.Longitude[i]]).add_to(SFC_map)
SFC_map


# In[8]:


#Save the bus stop map to html file
SFC_map.save('busmap.html')

#Save bus stop data with latitude and longitude to csv
busstop.to_csv('Bus.csv')


# In[9]:


#Use direct Google API JSON to scrap detail latitude and longtitude for employee addess
import requests
latitudeAdd=[]
longitudeAdd=[]
api_key = "AIzaSyD_sVpnDsJ8thEGRjhHrow2EyNswtRBmiw"
for i in range(0, employee.address.count()):
    api_response = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}'.format(employee.address[i], api_key))
    api_response_dict = api_response.json()
    if api_response_dict['status'] == 'OK':
        latitude = api_response_dict['results'][0]['geometry']['location']['lat']
        longitude = api_response_dict['results'][0]['geometry']['location']['lng']
        latitudeAdd.append(latitude)
        longitudeAdd.append(longitude)
latitudeAdd=pd.Series(latitudeAdd)
employee = pd.concat((employee, latitudeAdd.rename('Latitude')), axis=1)
longitudeAdd=pd.Series(longitudeAdd)
employee = pd.concat((employee, longitudeAdd.rename('Longitude')), axis=1)


# In[10]:


#Draw on the bus stop map the address of all employee with each address is represented in red circle
for i in range(0, employee.Latitude.count()):
    folium.features.Circle(radius=15,location=[employee.Latitude[i],employee.Longitude[i]],color='red',fill=True).add_to(SFC_map)
SFC_map


# In[11]:


#Save the bus stop map with employee addresses to html file
SFC_map.save('busmap_employee_add.html')

#Save bus stop data with employee addresses to csv
employee.to_csv('Employee.csv')


# ### Comments
# 1. There is one outlier on the bus route. It caused by the Google API can not find the exact latitude and longtitude.
# 2. Most of the employees live along the bus route and can use buses to go to work. However, there are still many employees live far from the bus route (NorthWest, West, SouthEast of the Mission Street). These employees can use connecting buses
# 3. There are 2 employees' addresses that Google API cannot find the exact. These addresses include both location and address. 
